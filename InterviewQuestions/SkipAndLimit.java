package InterviewQuestions;

import java.util.Arrays;
import java.util.List;

public class SkipAndLimit {

    public static void main(String[] args){
        List<Integer> list = Arrays.asList(5,13,21,13,27,59,59,34);

        //list.stream().limit(3).forEach(System.out::println);

        list.stream().skip(4).forEach(System.out::println);
    }
}
