package MindTree;

public class SingletonClass {

    private static volatile SingletonClass instance = null;

    private SingletonClass(){

    }
    public static SingletonClass getInstance(){
        if(instance==null){
            syncronized (SingletonClass.class){
                if(instance==null){
                    instance= new SingletonClass();
                }
            }

        }
        return instance;
    }
}
