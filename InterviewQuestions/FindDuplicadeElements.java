package InterviewQuestions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FindDuplicadeElements {

    public static void main(String[] args){
        List<Integer> list = Arrays.asList(5,13,21,13,27,59,59,34);

        Set<Integer> set = new HashSet<>();

        list.stream().filter( s -> !set.add(s)).collect(Collectors.toSet()).forEach(System.out::println);
    }
}
