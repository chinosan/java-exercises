package InterviewQuestions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class NoOfOcurrence {

    public static void main(String[] args){
        String sentence = "welcome to the Chena Tech Point and Chena Tech Point welcome you";

        List<String> res = Arrays.asList(sentence.split(""));

        Map<String, Long> result = res.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(result);
    }
}
